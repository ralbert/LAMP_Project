----
#Guía para la Instalación de LAMP en Ubuntu 16.4 usando AWS

----
## Instalación de Apache
Primeramente se actualizan los paquetes de nuestra máquina y posterior a ello se instalará Apache 

    sudo apt-get update
    sudo apt-get install apache2




----
## Instalación de MySQL
Como segundo paso se instalará MySQL con el siguiente comando

> **Nota:**
> Durante el proceso de instalación ud deberá definir un Usuario y contraseña root

     sudo apt-get install mysql-server

----
## Instalación de PHP
Mediante el siguiente comando se instalará PHP con algunos helpers para mod, mysql and mcrypt


     sudo apt-get install php libapache2-mod-php php-mcrypt php-mysql

----
## Modificar permisos 
Ya que la ruta /var/www/html directory no tendrá permisos de edición. Con el siguiente comando los vamos a habilitar.


     sudo chmod 777 /var/www/html/

----
## Probar la instancia
Cree un archivo index.php en la siguiente ruta /var/www/html/ y pegue en él la siguiente línea de código


    <?php 
    phpinfo();
Guarde los cambios y abra su instancia de AWS
    
    http://<ip-AWS>/index.php

----
## Link para la historia en Medium 

    https://medium.com/@rudycastromarn/lamp-en-aws-4ab24e4fc2a6
